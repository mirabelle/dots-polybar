#!/bin/sh

MENU="$(rofi -sep "|" -dmenu -i -p 'System' -width 12 -hide-scrollbar -line-padding 4 -padding 20 -lines 4 <<< " Lock| Logout| Reboot| Shutdown")"
            case "$MENU" in
                *Lock) xflock4 ;;
		*Logout) pkill -u $(whoami);;
                *Reboot) systemctl --no-ask-password reboot ;;
                *Shutdown) systemctl -i poweroff
            esac

#!/bin/sh

UPDATES=$(zypper lu 2>/dev/null | grep -c "v |")

if [ $UPDATES -gt 0 ]; then
  echo " $UPDATES"
fi

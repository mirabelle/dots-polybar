#!/bin/sh

PATH_BAT0="/sys/class/power_supply/BAT0"
PATH_BAT1="/sys/class/power_supply/BAT1"

if [ -d "$PATH_BAT0" ]; then
    bat0_level=$(cat "$PATH_BAT0/energy_now")
    bat0_max=$(cat "$PATH_BAT0/energy_full")
fi

if [ -d "$PATH_BAT1" ]; then
    bat1_level=$(cat "$PATH_BAT1/energy_now")
    bat1_max=$(cat "$PATH_BAT1/energy_full")
fi

    bat_level=$(("$bat0_level + $bat1_level"))
    bat_max=$(("$bat0_max + $bat1_max"))
    bat_percent=$(("$bat_level * 100 / $bat_max"))

    bat_status=$(sed ':a;N;s/\n/ /;ta' $PATH_BAT0/status $PATH_BAT1/status \
	    | sed -e '/\<Charging\>/c\' -e '/\<\>/!c\' )

# Low battery alert
if [ $bat_percent -lt 10 ] && [ $bat_status = '' ]; then

	feh -F --randomize $HOME/Pictures/Alert
fi

# Battery status
if [ $bat_percent -gt 98 ]; then

	echo "$bat_status 100%"
else
	
	echo "$bat_status $bat_percent%"
fi


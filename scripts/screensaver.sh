#!/bin/sh

MODE=$(grep "mode:" ~/.xscreensaver | awk '{print $NF}')

if [ $# -ne 0 ]; then

	if [ $MODE = "off" ]; then
		sed -i '/^mode:/ s/off/blank/' ~/.xscreensaver
	else 
		sed -i '/^mode:/ s/blank/off/' ~/.xscreensaver
	fi
	
	xscreensaver-command --restart
fi

if [ $MODE = "off" ]; then
	echo "%{F#1e88e5}%{F-}"
else
	echo ""
fi


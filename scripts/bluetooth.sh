#!/bin/sh

HEADPHONE="30:35:AD:E7:FE:E1" #Beats Studio³

if [ $# -ne 0 ]; then

	if bluetoothctl <<< "info $HEADPHONE" | grep -q "Connected: yes" ; then

		bluetoothctl <<< "disconnect $HEADPHONE"
		bluetoothctl <<< "power off"
	else
		bluetoothctl <<< "power on"
		sleep 1
		bluetoothctl <<< "connect $HEADPHONE"
	fi >>/dev/null

fi

if bluetoothctl <<< "info $HEADPHONE" | grep -q "Connected: yes" ; then

	echo "%{F#1e88e5}%{F-}"
else
	echo ""

fi

